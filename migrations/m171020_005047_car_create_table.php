<?php

use yii\db\Migration;

/**
 * Class m171020_005047_car
 */
class m171020_005047_car_create_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('car', [
            'id' => $this->primaryKey(),
            'car_modification_id' => $this->integer(11)->notNull(),
            'year' => $this->integer(4)->notNull()
        ], $tableOptions);


        $this->createIndex('idx-unique-car',
            'car',
            ['car_modification_id', 'year'],
            true
        );

        $this->addForeignKey(
            'fk-car-car_modification_id-car_modification-id',
            'car',
            'car_modification_id',
            'car_modification',
            'id',
            'CASCADE',
            'CASCADE'
        );

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-car-car_modification_id-car_modification-id', 'car');
        $this->dropTable('car');
    }
}
