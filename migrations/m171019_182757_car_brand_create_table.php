<?php

use yii\db\Migration;

/**
 * Class m171019_182757_car_brend
 */
class m171019_182757_car_brand_create_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('car_brand', [
            'id' => $this->primaryKey(),
            'title' => $this->string(55)->notNull(),
            'description' => $this->text(),
        ], $tableOptions);

        $this->createIndex('idx-car_brand-title', 'car_brand', 'title');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('car_brand');
    }
}
