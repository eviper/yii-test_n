<?php

use yii\db\Migration;

/**
 * Class m171022_143144_car_modification
 */
class m171020_004804_car_modification_create_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('car_modification', [
            'id' => $this->primaryKey(),
            'car_type_id' => $this->integer(11)->notNull(),
            'title' => $this->string(55)->notNull(),

        ], $tableOptions);

        $this->createIndex(
            'idx-unique-car_modification',
            'car_modification',
            ['title','car_type_id'],
            true
        );

        $this->addForeignKey(
            'fk-car_modification-car_type_id-car_type-id',
            'car_modification',
            'car_type_id',
            'car_type',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-car_modification-car_type_id-car_type-id', 'car_modification');
        $this->dropTable('car_modification');
    }
}
