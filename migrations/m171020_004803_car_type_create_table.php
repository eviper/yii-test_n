<?php

use yii\db\Migration;

/**
 * Class m171020_004803_car_type
 */
class m171020_004803_car_type_create_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('car_type', [
            'id' => $this->primaryKey(),
            'car_brand_id' => $this->integer(11)->notNull(),
            'title' => $this->string(55)->notNull(),

        ], $tableOptions);

        $this->createIndex(
            'idx-unique-car_type',
            'car_type',
            ['car_brand_id','title'],
            true
        );

        $this->addForeignKey(
            'fk-car_type-car_brand_id-car_brand-id',
            'car_type',
            'car_brand_id',
            'car_brand',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-car_type-car_brand_id-car_brand-id', 'car_type');
        $this->dropTable('car_type');
    }
}
