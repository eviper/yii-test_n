<?php

use yii\db\Migration;

/**
 * Class m171023_013228_image
 */
class m171023_013228_image_create_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('image', [
            'id' => $this->primaryKey(),
            'font' => $this->integer(1)->notNull(),
            'img' => $this->string(255)->notNull(),
            'extension' => $this->string(10)->notNull(),
            'message' => $this->string(55)->notNull(),
            'time' => $this->integer(11)->notNull(),
            'fontSize' => $this->integer(3)->notNull(),
            'color' => $this->string(7)->notNull(),
            'positionX' => $this->integer(3)->notNull(),
            'positionY' => $this->integer(3)->notNull(),
        ], $tableOptions);

        $this->createIndex('idx-image-message', 'image', 'message');
        $this->createIndex('idx-image-img', 'image', 'img');
        $this->createIndex('idx-image-font', 'image', 'font');

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('image');
    }
}
