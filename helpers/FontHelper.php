<?php


namespace app\helpers;

class FontHelper
{
    const FONT_MPPU = 1;
    const FONT_RED_VEVET = 2;
    const FONT_UNISPACE = 3;
    const FONT_THEYPRISHED = 4;

    private static $fontPathArray = [
        self::FONT_MPPU => 'MotionPicturePersonalUse/MotionPicturePersonalUse.ttf',
        self::FONT_RED_VEVET => 'RedVevet/RedVevet.ttf',
        self::FONT_UNISPACE => 'unispace/unispace.ttf',
        self::FONT_THEYPRISHED => 'TheyPerished/TheyPerished.ttf'
    ];

    private static $fontTitleArray = [
        self::FONT_MPPU => 'Motion Picture Personal Use',
        self::FONT_RED_VEVET => 'Red Vevet',
        self::FONT_UNISPACE => 'unispace',
        self::FONT_THEYPRISHED => 'TheyPerished'
    ];

    private $font;

    /**
     * FontHelper constructor.
     * @param $font
     */
    public function __construct($font)
    {
        $this->font = $font;
    }

    /**
     * @return string
     */
    public function fontFamily(){
        return self::$fontTitleArray[$this->font];
    }

    /**
     * @return string
     */
    public function fontSrc(){
        return self::$fontPathArray[$this->font];
    }


    /**
     * @return array
     */
    public static function fontTitle(){
        return self::$fontTitleArray;
    }
}