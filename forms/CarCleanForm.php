<?php

namespace app\forms;

use app\models\CarBrand;
use app\models\CarModification;
use app\models\CarType;
use yii\base\Model;

/**
 * Class CarCleanForm
 * @package app\forms
 *
 * @property string $brand
 * @property string $type
 * @property string $modification
 * @property string $year
 */
class CarCleanForm extends Model
{
    public $brand;
    public $type;
    public $modification;
    public $year;

    /** @var CarBrand $_brand */
    private $_brand;
    /** @var CarType $_type */
    private $_type;
    /** @var CarModification $_modification */
    private $_modification;


    /**
     * CarForm constructor.
     * @param CarBrand|null $brand
     * @param CarType|null $type
     * @param CarModification|null $modification
     * @param array $config
     */
    public function __construct(
        CarBrand $brand = null,
        CarType $type = null,
        CarModification $modification = null,
        $config = []
    )
    {
        $this->_brand = $brand;
        $this->_type = $type;
        $this->_modification = $modification;
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->brand = $this->getBrand() ? $this->getBrand()->title : null;
        $this->type = $this->getType() ? $this->getType()->title : null;
        $this->modification = $this->getModification() ? $this->getModification()->title : null;
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['brand', 'type', 'modification', 'year'], 'required'],
            [['brand', 'type', 'modification'], 'string', 'max' => 255],
            [['year'], 'integer', 'min' => 1900, 'max' => date("Y")],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'brand' => 'Brand',
            'type' => 'Type',
            'modification' => 'Modification',
            'year' => 'Year',
        ];
    }

    /**
     * @return CarBrand|null
     */
    private function getBrand()
    {
        return $this->_brand ? $this->_brand : null;
    }

    /**
     * @return CarType|null
     */
    private function getType()
    {
        return ($this->getBrand() && $this->_type) ? $this->_type : null;
    }

    /**
     * @return CarModification|null
     */
    private function getModification()
    {
        return ($this->getType() && $this->_modification) ? $this->_modification : null;
    }
}