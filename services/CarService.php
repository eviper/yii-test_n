<?php

namespace app\services;


use app\forms\CarCleanForm;
use app\models\Car;
use app\models\CarBrand;
use app\models\CarModification;
use app\models\CarType;
use app\repositories\CarBrandRepository;
use app\repositories\CarModificationRepository;
use app\repositories\CarRepository;
use app\repositories\CarTypeRepository;

class CarService
{
    private $brand;
    private $modification;
    private $type;
    private $car;

    public function __construct(
        CarBrandRepository $carBrand,
        CarTypeRepository $carType,
        CarModificationRepository $carModification,
        CarRepository $car
    )
    {
        $this->brand = $carBrand;
        $this->type = $carType;
        $this->modification = $carModification;
        $this->car = $car;
    }
//
//    public function create(RecruitData $recruitData, $orderDate, $contractDate, $recruitDate)
//    {
//        $employee = Employee::create(
//            $recruitData->firstName,
//            $recruitData->lastName,
//            $recruitData->address,
//            $recruitData->email
//        );
//        $contract = Contract::create($employee, $recruitData->lastName, $recruitData->firstName, $contractDate);
//        $recruit = Recruit::create($employee, Order::create($orderDate), $recruitDate);
//        $this->transactionManager->execute(function () use ($employee, $recruit, $contract) {
//            $this->employeeRepository->add($employee);
//            $this->contractRepository->add($contract);
//            $this->recruitRepository->add($recruit);
//        });
//        $this->eventDispatcher->dispatch(new EmployeeRecruitEvent($employee));
//        return $employee;
//    }
//

    public function createNew(CarCleanForm $form)
    {
        $brand = CarBrand::createNew($form->brand);
        $type = CarType::createNew($form->type);
        $modification = CarModification::createNew($form->modification);
        $car = Car::createNew($form->year);

        $transaction = \Yii::$app->db->beginTransaction();
        try {
            /** Brand */
            if (!$this->brand->findByTitle($brand->title))
                $this->brand->add($brand);

            /** Type */
            $type->car_brand_id = $this->brand->id;

            if (!$this->type->findBy(['title' => $type->title, 'car_brand_id' => $type->car_brand_id]))
                $this->type->add($type);

            /** Modification */
            $modification->car_type_id = $this->type->id;

            if (!$this->modification->findBy(['title' => $modification->title, 'car_type_id' => $modification->car_type_id]))
                $this->modification->add($modification);

            /** Car */
            $car->car_modification_id = $this->modification->id;

            if (!$this->car->findBy([
                'car_modification_id' => $car->car_modification_id,
                'year' => $car->year
            ]))
                $this->car->add($car);

            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
        return $car;
    }

}