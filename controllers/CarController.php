<?php

namespace app\controllers;

use app\forms\CarCleanForm;
use app\services\CarService;
use Yii;
use app\models\Car;
use app\models\search\CarSearch as CarSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CarController implements the CRUD actions for Car model.
 */
class CarController extends Controller
{
    private $carService;

    public function __construct($id, $module, CarService $carService, $config = [])
    {
        $this->carService = $carService;
        parent::__construct($id, $module, $config = []);
    }



    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Car models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CarSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Car model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * New Car model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionNew()
    {
        $form = new CarCleanForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $car = $this->carService->createNew($form);
            Yii::$app->session->setFlash('success', 'New car has been created.');
            return $this->redirect(['view', 'id' => $car->id]);
        }

        return $this->render('car-new', [
            'carCleanForm' => $form,
        ]);
    }

    /**
     * New Car model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $car = new Car;
        if ($car->load(Yii::$app->request->post()) && $car->save()) {
            Yii::$app->session->setFlash('success', 'Add new car.');
            return $this->redirect(['view', 'id' => $car->id]);
        }

        return $this->render('create', [
            'model' => $car,
        ]);
    }

    /**
     * Updates an existing Car model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Car model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    /**
     * Finds the Car model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Car the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Car::find()
                ->where(['car.id' => $id])->joinWith([
                    'carModification',
                    'carModification.carType',
                    'carModification.carType.carBrand'
                ])->one()) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


}
