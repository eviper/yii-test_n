window.onload = function () {


    var vueScript = new Vue({
        el: '.carCreate',
        data: {
            brands: data,
            id_brand: this.data.id_brand,
            id_type: this.data.id_type,
            id_modification: this.data.id_modification
        },
        computed: {
            valueBrand: function () {
                return this.brands['store'][this.id_brand];
            },
            isBrand: function () {
                return this.valueBrand !== undefined;
            },
            hasType: function () {
                return this.isBrand && this.valueBrand['types'] !== undefined;
            },
            hasModification: function () {
                return this.hasType && this.valueBrand['modification'][this.id_type] !== undefined;
            }
        }
    });

};