<?php

$webroot = '@app/web';

return [
    '@uploadroot' =>  $webroot . '/uploads/',
    '@uploads' =>  '/uploads/',
    '@fontroot' =>  $webroot . '/fonts/',
    '@font' =>  '/fonts/',
    '@bower' => '@vendor/bower-asset',
    '@npm'   => '@vendor/npm-asset',

];