<?php

return [
    // the module class
    'class' => 'kartik\social\Module',

    // the global settings for the facebook plugins widget
    'facebook' => [
        'appId' => '1445019328944882',
        'app_secret' => '',
    ],

    // the global settings for the twitter plugins widget
    'twitter' => [
        'screenName' => 'TWITTER_SCREEN_NAME'
    ],

    // the global settings for the twitter widget
    'vk' => [
        'apiId' => 'API_ID',
    ],
];