<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CarModification */

$this->title = 'Create Car Modification';
$this->params['breadcrumbs'][] = ['label' => 'Car Modifications', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="car-modification-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
