<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CarModification */

$this->title = 'Update Car Modification: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Car Modifications', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="car-modification-update">

    <h1><?= $this->title ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
