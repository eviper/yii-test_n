<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\CarModification */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="car-modification-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'class' =>'carCreate'
        ]
    ]); ?>

    <?= $form->field($model, 'car_brand')->dropDownList(["{{brand.title}}"],
        ['v-model' => 'id_brand',
            'options' => [
                [
                    'v-for' => '(brand, id_brand) in brands["store"]',
                    'v-bind:value' => 'id_brand'
                ]
            ]]) ?>
    <div v-bind:class="{ carCreate__error: !hasType }">
        <?= $form->field($model, 'car_type_id')->dropDownList(["{{type}}"],
            ['v-model' => 'id_type', 'v-if' => 'hasType', 'options' => [
                [
                    'v-for' => '(type, id_type) in brands["store"][id_brand].types',
                    'v-bind:value' => 'id_type'
                ]
            ]]); ?>
        <div v-if="!hasType">
            This brand does not have any type.
            <?= Html::a('Add type', Url::to(['car-type/create'])); ?>
        </div>
    </div>

    <?= $form->field($model, 'title',
        ['options' => ['v-if' => 'hasType']])->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success', ':disabled' => '!hasType']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <script type="text/javascript">
        var data = <?= json_encode($model->getCarBrandAndType() , JSON_FORCE_OBJECT) ?>;
    </script>
</div>
