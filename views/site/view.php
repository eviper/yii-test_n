<?php

use yii\helpers\Html;
use kartik\social\VKPlugin;
use kartik\social\FacebookPlugin;
use kartik\social\TwitterPlugin;

/* @var $this yii\web\View */
/* @var $model app\models\Image */

$this->title = $model->img;
$this->params['breadcrumbs'][] = ['label' => 'Images', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="image-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Image', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="image">
        <?= Html::img(Yii::getAlias('@uploads/')
            . $model->time . '/' . $model->img . '.' . $model->extension,
            ['class' => 'image__img'])  ?>
    </div>
    <div class="image-view__share">
        <div class="image-view__share_vk">
            <?= VKPlugin::widget([
                'type' => VKPlugin::SHARE,
                'options' => [
                    'type' => 'round',
                    'text' => 'Share',
                    'eng' => 1
                ]
            ]);
            ?>
        </div>
        <div class="image-view__share_vk">
            <?= TwitterPlugin::widget([
                'type'=>TwitterPlugin::SHARE,
                'settings' =>
                    ['size'=>'default']
            ]);
            ?>
        </div>
    </div>

    <?= FacebookPlugin::widget([
            'type'=>FacebookPlugin::SHARE,
            'settings' =>
                ['size'=>'small', 'layout'=>'button_count', 'mobile_iframe'=>'false']
        ]);
    ?>


</div>
