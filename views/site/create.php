<?php

use yii\helpers\Html;
use app\models\Image;

/* @var $this yii\web\View */
/* @var $model Image */

$model->fontSize = Image::DEFAULT_FONT_SIZE;
$model->color = Image::DEFAULT_COLOR;
$model->positionX = Image::DEFAULT_POS_X;
$model->positionY = Image::DEFAULT_POS_Y;

$this->title = 'Create Image';
$this->params['breadcrumbs'][] = ['label' => 'Images', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="image-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
