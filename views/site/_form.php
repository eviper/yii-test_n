<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\color\ColorInput;

/* @var $this yii\web\View */
/* @var $model app\models\Image */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="image-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

    <?= $form->field($model, 'font')->dropDownList(\app\helpers\FontHelper::fontTitle()) ?>

    <?= $form->field($model, 'color')->widget(ColorInput::classname(), [
        'options' => ['placeholder' => 'Select color ...'],
    ]); ?>

    <?= $form->field($model, 'fontSize')->textInput(['type' => 'number', 'min' => 1, 'max' => 120]) ?>

    <?php if (!$model->img): ?>
        <?= $form->field($model, 'imageFile')->fileInput() ?>
    <?php endif; ?>

    <?= $form->field($model, 'positionX')->textInput(['type' => 'number', 'min' => 0, 'max' => 800]) ?>

    <?= $form->field($model, 'positionY')->textInput(['type' => 'number', 'min' => 0, 'max' => 800]) ?>

    <?= $form->field($model, 'message')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
