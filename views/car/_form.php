<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\Car */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="car-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'class' =>'carCreate'
         ]
    ]); ?>



    <?= $form->field($model, 'car_brand')->dropDownList(["{{brand.title}}"],
        ['v-model' => 'id_brand',
            'options' => [
                [
                    'v-for' => '(brand, id_brand) in brands["store"]',
                    'v-bind:value' => 'id_brand'
                ]
            ]]) ?>
    <div v-bind:class="{ carCreate__error: !hasType }">
        <?= $form->field($model, 'car_type')->dropDownList(["{{type}}"],
            ['v-model' => 'id_type', 'v-if' => 'hasType', 'options' => [
                [
                    'v-for' => '(type, id_type) in valueBrand.types',
                    'v-bind:value' => 'id_type'
                ]
            ]]); ?>
        <div v-if="!hasType">
            This brand does not have any type.
            <?= Html::a('Add type', Url::to(['car-type/create'])); ?>
        </div>
    </div>
    <div v-bind:class="{ carCreate__error: !hasModification }">
        <?= $form->field($model, 'car_modification_id')->dropDownList(["{{modification}}"],
            ['v-model' => 'id_modification', 'v-if' => 'hasModification', 'options' => [
                [
                    'v-for' => '(modification, id_modification) in valueBrand.modification[id_type]',
                    'v-bind:value' => 'id_modification'
                ]
            ]]); ?>
        <div v-if="!hasModification">
            This type does not have any modifications.
            <?= Html::a('Add modification', Url::to(['car-modification/create'])); ?>
        </div>
    </div>

    <?= $form->field($model, 'year',
        ['options' => ['v-if' => 'hasType']])->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success', ':disabled' => '!hasModification']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <script type="text/javascript">
        var data = <?= json_encode($model->getCarBrandAndType(), JSON_FORCE_OBJECT) ?>;
    </script>
</div>
