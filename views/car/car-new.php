<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $carCleanForm app\forms\CarCleanForm */
/* @var $form yii\widgets\ActiveForm */
/* @var $this yii\web\View */
/* @var $model app\models\Car */

$this->title = 'Create New Car';
$this->params['breadcrumbs'][] = ['label' => 'Cars', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="new-car-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="car-brand-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($carCleanForm, 'brand')->textInput(['maxlength' => true]) ?>

        <?= $form->field($carCleanForm, 'type')->textInput(['maxlength' => true]) ?>

        <?= $form->field($carCleanForm, 'modification')->textInput(['maxlength' => true]) ?>

        <?= $form->field($carCleanForm, 'year')->textInput(['maxlength' => true]) ?>


        <div class="form-group">
            <?= Html::submitButton('Create', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
