<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CarType as CarTypeModel;

/**
 * CarType represents the model behind the search form of `app\models\CarType`.
 */
class CarTypeSearch extends CarTypeModel
{
    public $brand;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'car_brand_id'], 'integer'],
            [['title', 'brand'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CarTypeModel::find()
            ->select(['car_type.id','car_type.title','car_brand_id'])
            ->joinWith('carBrand');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'title',
                'brand' => [
                    'asc' => ['car_brand.title' => SORT_ASC],
                    'desc' => ['car_brand.title' => SORT_DESC],
                    'default' => SORT_ASC
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'brand' => $this->carBrand->title,
            'car_brand_id' => $this->car_brand_id,
        ]);

        $query->andFilterWhere(['like', 'car_type.title', $this->title])
            ->andFilterWhere(['like', 'car_brand.title', $this->brand]);

        return $dataProvider;
    }
}
