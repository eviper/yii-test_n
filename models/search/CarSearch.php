<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Car as CarModel;

/**
 * Car represents the model behind the search form of `app\models\Car`.
 */
class CarSearch extends CarModel
{
    public $brand;
    public $type;
    public $modification;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'car_modification_id'], 'integer'],
            [['year', 'brand', 'type', 'modification' ], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CarModel::find()
            ->select(['car.id','car.year','car_modification_id'])
            ->with(['carModification'])->joinWith(['carModification.carType','carModification.carType.carBrand']);;

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'year',
                'type' => [
                    'asc' => ['car_type.title' => SORT_ASC],
                    'desc' => ['car_type.title' => SORT_DESC],
                    'default' => SORT_ASC
                ],
                'brand' => [
                    'asc' => ['car_brand.title' => SORT_ASC],
                    'desc' => ['car_brand.title' => SORT_DESC],
                    'default' => SORT_ASC
                ],
                'modification' => [
                    'asc' => ['car_modification.title' => SORT_ASC],
                    'desc' => ['car_modification.title' => SORT_DESC],
                    'default' => SORT_ASC
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'car_modification_id' => $this->car_modification_id,
        ]);

        $query->andFilterWhere(['like', 'car_modification.title', $this->modification])
            ->andFilterWhere(['like', 'car_type.title', $this->type])
            ->andFilterWhere(['like', 'car_brand.title', $this->brand])
            ->andFilterWhere(['like', 'car.year', $this->year]);

        return $dataProvider;
    }
}
