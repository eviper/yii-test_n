<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CarModification;

/**
 * CarModificationSearch represents the model behind the search form of `app\models\CarModification`.
 */
class CarModificationSearch extends CarModification
{
    public $brand;
    public $type;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'car_type_id'], 'integer'],
            [['title','brand', 'type'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CarModification::find()
            ->select(['car_modification.id','car_modification.title','car_type_id'])
            ->with(['carType'])->joinWith(['carType.carBrand']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'title',
                'type' => [
                    'asc' => ['car_type.title' => SORT_ASC],
                    'desc' => ['car_type.title' => SORT_DESC],
                    'default' => SORT_ASC
                ],
                'brand' => [
                    'asc' => ['car_brand.title' => SORT_ASC],
                    'desc' => ['car_brand.title' => SORT_DESC],
                    'default' => SORT_ASC
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        // grid filtering conditions
        $query->andFilterWhere([
            'type' => $this->carType->title,
            'brand' => $this->carType->carBrand->title,
            'car_type_id' => $this->car_type_id
        ]);

        $query->andFilterWhere(['like', 'car_modification.title', $this->title])
            ->andFilterWhere(['like', 'car_type.title', $this->type])
            ->andFilterWhere(['like', 'car_brand.title', $this->brand]);

        return $dataProvider;
    }
}
