<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "car_brand".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 *
 * @property CarType[] $carTypes
 * @property CarModification[] $carModifications
 */
class CarBrand extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'car_brand';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['description'], 'string'],
            [['title'], 'string', 'max' => 55],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Brand',
            'description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarTypes()
    {
        return $this->hasMany(CarType::className(), ['car_brand_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarModifications()
    {
        return $this->hasMany(CarModification::className(), ['car_type_id' => 'id'])->via('carTypes');
    }


    /**
     * @param $title
     * @return CarBrand
     */
    public static function createNew($title)
    {
        $carBrand = new self();
        $carBrand->title = $title;
        return $carBrand;
    }


}
