<?php

namespace app\models;
/**
 * This is the model class for table "car".
 *
 * @property int $id
 * @property int $car_modification_id
 * @property int $year
 *
 * @property CarModification $carModification
 *
 * @property int $car_brand
 * @property [] carBrandAndType
 *
 * @property string brand
 * @property string type
 * @property string modification
 * @property string fullTitle
 */
class Car extends \yii\db\ActiveRecord
{

    public $car_brand;
    public $car_type;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'car';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['car_modification_id', 'year'], 'required'],
            [['car_modification_id', 'year'], 'integer'],
            [['car_modification_id', 'year'], 'unique', 'targetAttribute' => ['car_modification_id', 'year']],
            [['car_modification_id'], 'exist', 'skipOnError' => true, 'targetClass' => CarModification::className(), 'targetAttribute' => ['car_modification_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'car_modification_id' => 'Car Modification ID',
            'year' => 'Year',
            'car_brand' => 'Car Brand',
            'car_type' => 'Car Type',
        ];
    }

    /**
     * @return string
     */
    public function getBrand()
    {
        return $this->carModification->carType->carBrand->title;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->carModification->carType->title;
    }

    /**
     * @return string
     */
    public function getModification()
    {
        return $this->carModification->title;
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarModification()
    {
        return $this->hasOne(CarModification::className(), ['id' => 'car_modification_id']);
    }

    public function getFullTitle(){
        return  $this->brand . ' ' . $this->type. ' ' . $this->modification;
    }

    /**
     * @return array
     */
    public function getCarBrandAndType(){
        $result = [];
        $carBrands = CarBrand::find()
            ->with(['carTypes','carModifications'])
            ->all();
        foreach ($carBrands as $brands){
            $result['store'][$brands->id]['title']= $brands->title;
            foreach ($brands->carTypes as $type){
                $result['store'][$brands->id]['types'][$type->id] = $type->title;

                foreach ($type->carModifications as $modification){
                    $result['store'][$brands->id]['modification'][$type->id][$modification->id] = $modification->title;
                }
            }
        }

        if ($this->car_modification_id){
            $result['id_type'] = $this->carModification->car_type_id;
            $result['id_brand'] = $this->carModification->carType->car_brand_id;
            $result['id_modification'] = $this->car_modification_id;
        } else {
            $result['id_type'] = 1;
            $result['id_brand'] = 1;
            $result['id_modification'] = 0;
        }
        return $result;
    }

    /**
     * @param $year
     * @return Car
     */
    public static function createNew($year)
    {
        $car = new self();
        $car->year = $year;
        return $car;
    }
}
