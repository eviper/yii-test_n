<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "car_type".
 *
 * @property int $id
 * @property int $car_brand_id
 * @property string $title
 *
 * @property CarModification[] $carModifications
 * @property CarBrand $carBrand
 */
class CarType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'car_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['car_brand_id'], 'required'],
            [['car_brand_id'], 'integer'],
            [['title'], 'string', 'max' => 55],
            [['car_brand_id', 'title'], 'unique', 'targetAttribute' => ['car_brand_id', 'title']],
            [['car_brand_id'], 'exist', 'skipOnError' => true, 'targetClass' => CarBrand::className(), 'targetAttribute' => ['car_brand_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'car_brand_id' => 'Car Brand ID',
            'title' => 'Type',
        ];
    }

    /**
     * @return string
     */
    public function getBrand()
    {
        return $this->carBrand->title;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarModifications()
    {
        return $this->hasMany(CarModification::className(), ['car_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarBrand()
    {
        return $this->hasOne(CarBrand::className(), ['id' => 'car_brand_id']);
    }

    /**
     * @param $title
     * @return CarType
     */
    public static function createNew($title)
    {
        $carType = new self();
        $carType->title = $title;
        return $carType;
    }
}
