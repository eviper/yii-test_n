<?php

namespace app\models;

use app\helpers\FontHelper;
use Yii;
use yii\db\Exception;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;
use yii\imagine\Image as Imagine;

/**
 * This is the model class for table "image".
 *
 * @property int $id
 * @property int $font
 * @property string $img
 * @property string $extension
 * @property string $message
 * @property int $time
 * @property int $fontSize
 * @property string $color
 * @property int $positionX
 * @property int $positionY
 *
 */
class Image extends \yii\db\ActiveRecord
{

    const SCENARIO_SAVE_IMAGE_DB = 'saveImageDB';
    const ORIGIN_FILE_SUFFIX = '_origin';
    const DEFAULT_COLOR = '#000000';
    const DEFAULT_FONT_SIZE = 23;
    const DEFAULT_POS_X = 0;
    const DEFAULT_POS_Y = 0;

    /**
     * @var UploadedFile
     */
    public $imageFile;

    /** @var string $path */
    private $path;

    /** @var string $fullPath */
    private $fullPath;

    /** @var string $rootPath */
    private $rootPath;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'image';
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_SAVE_IMAGE_DB] =
            ['font', 'img', 'message', 'color', 'positionX', 'positionY', 'fontSize', 'time'];

        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['font'], 'required'],

            [['font', 'time'], 'integer'],
            [['message'], 'string', 'max' => 55],
            [['img'], 'string', 'max' => 255],
            [['color'], 'string', 'min' => 3, 'max' => 7],
            [['positionX', 'positionY'], 'integer', 'min' => 0 , 'max' => 800],
            [['fontSize'], 'integer', 'min' => 1, 'max' => 120],
            ['imageFile', 'image', 'extensions' => 'png, jpg', 'skipOnEmpty' => false,
                'minWidth' => 10, 'maxWidth' => 800,
                'minHeight' => 10, 'maxHeight' => 800,
                'maxSize' => 500 * 1024
            ],

            [['color'], 'default', 'value' => '#000000'],
            [['positionX', 'positionY'], 'default', 'value' => 0],
            [['fontSize'], 'default', 'value' => 23],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'img' => 'Img',
            'imageFile' => 'Image',
            'font' => 'Font',
            'message' => 'Message',
            'extension' => 'Extension',
            'fontSize' => 'Font size',
            'color' => 'Font color',

        ];
    }


    public function upload()
    {
        if ($this->validate()) {
            $this->time = time();
            $this->path = $this->time . '/';
            $this->rootPath = Yii::getAlias('@uploadroot') . $this->path;
            FileHelper::createDirectory($this->rootPath);
            $this->fullPath =
                $this->rootPath
                . $this->imageFile->baseName
                . self::ORIGIN_FILE_SUFFIX . '.'
                . $this->imageFile->extension;
            $this->imageFile->saveAs($this->fullPath);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Set text in image
     *
     * @return bool
     */
    public function setText()
    {
        $font = new FontHelper($this->font);
        $fontFile = Yii::getAlias('@fontroot') . $font->fontSrc();

        /** Set text to image */
        $img = Imagine::text(
            $this->fullPath,
            $this->message,
            $fontFile,
            [
                $this->positionX,
                $this->positionY
            ],
            [
                'size' => $this->fontSize,
                'color' => $this->color
            ]
        );

        $newPath = $this->rootPath . $this->img . '.' . $this->extension;
        $img->save($newPath);
        return (file_exists($newPath)) ? true : false;
    }

    /**
     * Save model in db
     *
     *
     * @return bool
     */
    public function saveImageDB()
    {
        $this->scenario = self::SCENARIO_SAVE_IMAGE_DB;
        $this->img = $this->imageFile->baseName;
        $this->extension = $this->imageFile->extension;
        return $this->save();
    }

    public function updateImage()
    {
        $this->scenario = self::SCENARIO_SAVE_IMAGE_DB;
        $this->rootPath = Yii::getAlias('@uploadroot') . $this->time . '/';
        $this->fullPath =
            $this->rootPath
            . $this->img
            . self::ORIGIN_FILE_SUFFIX . '.'
            . $this->extension;

        return $this->save();
    }
}
