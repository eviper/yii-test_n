<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "car_modification".
 *
 * @property int $id
 * @property int $car_type_id
 * @property string $title
 *
 * @property Car[] $cars
 * @property CarType $carType
 *
 * @property int $car_brand
 */
class CarModification extends \yii\db\ActiveRecord
{
    /** @var int $car_brand */
    public $car_brand;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'car_modification';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['car_type_id', 'title'], 'required'],
            [['car_type_id'], 'integer'],
            [['title'], 'string', 'max' => 55],
            [['title', 'car_type_id'], 'unique', 'targetAttribute' => ['title', 'car_type_id']],
            [['car_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => CarType::className(), 'targetAttribute' => ['car_type_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'car_type_id' => 'Car Type ID',
            'title' => 'Modification',
            'CarTypeTitle' => 'Type',
            'CarBrandTitle' => 'Brand'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCars()
    {
        return $this->hasMany(Car::className(), ['car_modification_id' => 'id']);
    }


    /**
     * @return string
     */
    public function getType()
    {
        return $this->carType->title;
    }


    /**
     * @return string
     */
    public function getBrand()
    {
        return $this->carType->carBrand->title;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarType()
    {
        return $this->hasOne(CarType::className(), ['id' => 'car_type_id']);
    }


    /**
     * @return array
     */
    public function getCarBrandAndType(){
        $result = [];
        $carBrands = CarBrand::find()
            ->with(['carTypes'])
            ->all();
        foreach ($carBrands as $brands){
            $result['store'][$brands->id]['title']= $brands->title;
            foreach ($brands->carTypes as $type){
                $result['store'][$brands->id]['types'][$type->id] = $type->title;
            }
        }
        if ($this->car_type_id){
            $result['id_type'] = $this->car_type_id;
            $result['id_brand'] = $this->carType->carBrand->id;
        } else {
            $result['id_type'] = 1;
            $result['id_brand'] = 1;
        }
        return $result;
    }

    /**
     * @param $title
     * @return CarModification
     */
    public static function createNew($title)
    {
        $carModification = new self();
        $carModification->title = $title;
        return $carModification;
    }
}
