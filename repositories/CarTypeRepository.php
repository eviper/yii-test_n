<?php

namespace app\repositories;

use app\models\CarType;

/**
 * Class CarTypeRepository
 * @package app\repositories
 *
 * @property integer $id
 */
class CarTypeRepository
{
    public $id;

    /**
     * @param array $params
     * @return null|CarType
     */
    public function findBy($params = [])
    {
        if ($model = CarType::findOne($params))
            $this->getId($model);
        return $model;
    }

    /**
     * @param CarType $carType
     */
    public function add(CarType $carType)
    {
        if (!$carType->getIsNewRecord()) {
            throw new \RuntimeException('Adding existing model.');
        }
        if (!$carType->insert(false)) {
            throw new \RuntimeException('Saving error.');
        }
        $this->id = $carType->id;
    }

    /**
     * @param CarType $carType
     */
    public function save(CarType $carType)
    {
        if ($carType->getIsNewRecord()) {
            throw new \RuntimeException('Saving new model.');
        }
        if ($carType->update(false) === false) {
            throw new \RuntimeException('Saving error.');
        }
    }

    /**
     * @param CarType $carType
     */
    public function delete(CarType $carType)
    {
        if (!$carType->delete()) {
            throw new \RuntimeException('Deleting error.');
        }
    }

    private function getId($model)
    {
        $this->id = $model->id;
    }
}