<?php

namespace app\repositories;


use app\models\CarBrand;

/**
 * Class CarBrandRepository
 * @package app\repositories
 *
 * @property integer $id
 */
class CarBrandRepository
{
    public $id;

    /**
     * @param $title
     * @return null|CarBrand
     */
    public function findByTitle($title)
    {
        if ($model = CarBrand::findOne(['title' => $title]))
            $this->getId($model);
        return $model;
    }

    /**
     * @param CarBrand $carBrand
     */
    public function add(CarBrand $carBrand)
    {
        if (!$carBrand->getIsNewRecord()) {
            throw new \RuntimeException('Adding existing model.');
        }
        if (!$carBrand->insert(false)) {
            throw new \RuntimeException('Saving error.');
        }
        $this->id = $carBrand->id;
    }

    /**
     * @param CarBrand $carBrand
     */
    public function save(CarBrand $carBrand)
    {
        if ($carBrand->getIsNewRecord()) {
            throw new \RuntimeException('Saving new model.');
        }
        if ($carBrand->update(false) === false) {
            throw new \RuntimeException('Saving error.');
        }
    }

    /**
     * @param CarBrand $carBrand
     */
    public function delete(CarBrand $carBrand)
    {
        if (!$carBrand->delete()) {
            throw new \RuntimeException('Deleting error.');
        }
    }

    private function getId($model)
    {
        $this->id = $model->id;
    }
}