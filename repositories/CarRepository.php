<?php

namespace app\repositories;


use app\models\Car;
use Imagine\Exception\RuntimeException;
use yii\web\NotFoundHttpException;

/**
 * Class CarRepository
 * @package app\repositories
 */
class CarRepository
{


    /**
     * @param array $params
     * @return null|Car
     */
    public function findBy($params = [])
    {
        if ($model = Car::findOne($params))
            throw new RuntimeException('This car is already exist.');
        return $model;
    }

    /**
     * @param Car $car
     * @return Car
     */
    public function add(Car $car)
    {
        if (!$car->getIsNewRecord()) {
            throw new \RuntimeException('Adding existing model.');
        }
        if (!$car->insert(false)) {
            throw new \RuntimeException('Saving error.');
        }
        return $car;
    }

    /**
     * @param Car $car
     */
    public function save(Car $car)
    {
        if ($car->getIsNewRecord()) {
            throw new \RuntimeException('Saving new model.');
        }
        if ($car->update(false) === false) {
            throw new \RuntimeException('Saving error.');
        }
    }

    /**
     * @param Car $car
     */
    public function delete(Car $car)
    {
        if (!$car->delete()) {
            throw new \RuntimeException('Deleting error.');
        }
    }

}