<?php

namespace app\repositories;


use app\models\CarModification;

/**
 * Class CarModificationRepository
 * @package app\repositories
 *
 * @property integer $id
 */
class CarModificationRepository
{

    public $id;


    /**
     * @param array $params
     * @return null|CarModification
     */
    public function findBy($params = [])
    {
        if ($model = CarModification::findOne($params))
            $this->getId($model);
        return $model;
    }

    /**
     * @param CarModification $model
     */
    public function add(CarModification $model)
    {
        if (!$model->getIsNewRecord()) {
            throw new \RuntimeException('Adding existing model.');
        }
        if (!$model->insert(false)) {
            throw new \RuntimeException('Saving error.');
        }
        $this->id = $model->id;
    }

    /**
     * @param CarModification $model
     */
    public function save(CarModification $model)
    {
        if ($model->getIsNewRecord()) {
            throw new \RuntimeException('Saving new model.');
        }
        if ($model->update(false) === false) {
            throw new \RuntimeException('Saving error.');
        }
    }

    /**
     * @param CarModification $model
     */
    public function delete(CarModification $model)
    {
        if (!$model->delete()) {
            throw new \RuntimeException('Deleting error.');
        }
    }

    private function getId($model)
    {
        $this->id = $model->id;
    }
}